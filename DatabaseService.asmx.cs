﻿using FinalProject_ASP.main.models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Web.Services;

namespace FinalProject_ASP
{
    /// <summary>
    /// Descripción breve de WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class DatabaseService : WebService
    {

        //
        // Fabricantes
        [WebMethod]
        public List<Fabricante> GetFabricantes()
        {
            return Database.GetInstanceOf().GetFabricantes();
        }

        [WebMethod]
        public int GetFabricanteIdByNombre(string nombre)
        {
            return Database.GetInstanceOf().GetFabricanteIdByNombre(nombre);
        }

        [WebMethod]
        public string GetFabricanteNombreById(int id)
        {
            return Database.GetInstanceOf().GetFabricanteNombreById(id);
        }

        [WebMethod]
        public bool AddFabricante(Fabricante fabricante)
        {
            return Database.GetInstanceOf().AddFabricante(fabricante);
        }

        [WebMethod]
        public bool UpdateFabricante(int id, string nombre)
        {
            return Database.GetInstanceOf().UpdateFabricante(id, nombre);
        }

        [WebMethod]
        public bool DeleteFabricante(int id)
        {
            return Database.GetInstanceOf().DeleteFabricante(id);
        }

        //
        // Productos
        [WebMethod]
        public List<Producto> GetProductos()
        {
            return Database.GetInstanceOf().GetProductos();
        }

        [WebMethod]
        public Producto GetProductoById(int id)
        {
            return Database.GetInstanceOf().GetProductoById(id);
        }

        [WebMethod]
        public bool AddProduct(Producto producto)
        {
            return Database.GetInstanceOf().AddProduct(producto);
        }

        [WebMethod]
        public bool UpdateProducto(int id, string nombre, double precio, int fabricante)
        {
            return Database.GetInstanceOf().UpdateProducto(id, nombre, precio, fabricante);
        }

        [WebMethod]
        public bool DeleteProducto(int id)
        {
            return Database.GetInstanceOf().DeleteProducto(id);
        }

        //
        // Search
        //
        [WebMethod]
        public MySqlDataReader ReaderForSearch(string table, string column, string value)
        {
            return Database.GetInstanceOf().ReaderForSearch(table, column, value);
        }

        //
        // Administradores
        [WebMethod]
        public bool Login(string usuario, string contrasena)
        {
            return Database.GetInstanceOf().Login(usuario, contrasena);
        }

        [WebMethod]
        public bool UserExists(string username)
        {
            return Database.GetInstanceOf().UserExists(username);
        }
    }
}
