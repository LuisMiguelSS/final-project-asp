﻿using System.Web.Services;

namespace FinalProject_ASP
{
    /// <summary>
    /// Descripción breve de WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class SessionService : WebService
    {

        [WebMethod]
        public string GetUser()
        {
            return IsUserLoggedIn() ? Session["username"].ToString() : "";
        }
        [WebMethod]
        public bool IsUserLoggedIn()
        {
            return Session["username"] != null;
        }

        [WebMethod]
        public void Start(string username)
        {
            Session["username"] = username;
        }

        [WebMethod]
        public void Close()
        {
            Session["username"] = null;
        }
    }
}
