﻿$(document).ready(function () {
    // Show/hide password field at login
    $("#passwordGroup .input-group-append a").on('click', function (event) {
        event.preventDefault();

        if ($('#passwordInput').attr("type") == "text") {

            $('#passwordInput').attr('type', 'password');
            $('#passwordGroup .input-group-append a i').addClass("fa-eye-slash");
            $('#passwordGroup .input-group-append a i').removeClass("fa-eye");

        } else if ($('#passwordInput').attr("type") == "password") {

            $('#passwordInput').attr('type', 'text');
            $('#passwordGroup .input-group-append a i').removeClass("fa-eye-slash");
            $('#passwordGroup .input-group-append a i').addClass("fa-eye");

        }
    });

    // TreeView
    var toggler = document.getElementsByClassName("caret");
    var i;

    for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function () {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
    }
    
});