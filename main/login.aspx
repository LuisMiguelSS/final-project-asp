﻿<%@ Page Title="Inicio de sesión - Tienda" Language="C#" MasterPageFile="~/main/template.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="FinalProject_ASP.main.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid col-sm-8 col-md-6 col-lg-5 col-xl-4">
        <div class="card mt-4">
            <article class="card-body">
                <h4 class="card-title text-center mb-4 mt-1">Inicia Sesión</h4>
                <hr>
                <p class="text-center">Introduce tu usuario y contraseña para continuar.</p>

                <!-- Username -->
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                        </div>
                        <asp:TextBox ID="usernameInput" class="form-control" placeholder="Usuario" type="text" runat="server"></asp:TextBox>
                    </div>

                    <asp:RequiredFieldValidator ID="RequiredUsernameValidator" runat="server" ErrorMessage="¡Debes introducir el usuario!" ControlToValidate="usernameInput" CssClass="text-danger"></asp:RequiredFieldValidator>
                </div>

                <!-- Password -->
                <div class="form-group">
                    <div class="input-group" id="passwordGroup">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-lock"></i></span>
                        </div>
                        <asp:TextBox ID="passwordInput" class="form-control" placeholder="Contraseña" type="password" TextMode="Password" runat="server" ClientIDMode="Static"></asp:TextBox>
                        <!-- Eye -->
                        <div class="input-group-append">
                            <a href="#" class="btn input-group-text"><i class="fa fa-eye-slash"></i></a>
                        </div>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredPasswordValidator" runat="server" ErrorMessage="¡Debes introducir la contraseña!" ControlToValidate="passwordInput" CssClass="text-danger"></asp:RequiredFieldValidator>
                </div>

                <!-- Submit -->
                <div class="form-group">
                    <asp:Button type="submit" class="btn btn-primary btn-block" runat="server" OnClick="TryLogin" Text="Entrar"></asp:Button>
                </div>

            </article>
        </div>
    </div>

</asp:Content>
