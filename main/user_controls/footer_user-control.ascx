﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="footer_user-control.ascx.cs" Inherits="FinalProject_ASP.main.user_controls.WebUserControl2" %>
<footer class="page-footer font-small">
    <div class="footer-copyright text-center py-3">
        © 2020 Copyright -
            <a href="https://luismiguelss.github.io/me">Luis Miguel Soto Sánchez</a>
    </div>
</footer>
