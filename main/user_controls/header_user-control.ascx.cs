﻿using System;

namespace FinalProject_ASP.main.user_controls
{
    public partial class HeaderUserControl : System.Web.UI.UserControl
    {
        //
        // Attributes
        //
        public SessionService session { get; set; }

        //
        // Methods
        //
        [System.Web.Services.WebMethod]
        protected void Logout(object sender, EventArgs e)
        {
            session.Close();
            Response.Redirect("login.aspx");
        }

        //
        // Listeners/events
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            session = new SessionService();

            if (session.IsUserLoggedIn())
            {
                navigation.Style.Add("background", "#ffa16b !important");
                navigation.Attributes["class"] = navigation.Attributes["class"].Replace("navbar-dark", "navbar-light");
            }
            else
                logoutNavigation.Visible = false;

            welcomeText.InnerText = "¡Bienvenido/a, " + (session.IsUserLoggedIn() ? session.GetUser() + "!" : "Invitado!");
        }
    }
}