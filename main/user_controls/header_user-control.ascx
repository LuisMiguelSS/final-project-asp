﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="header_user-control.ascx.cs" Inherits="FinalProject_ASP.main.user_controls.HeaderUserControl" %>
<header>
    <nav id="navigation" class="navbar navbar-expand-sm bg-dark navbar-dark" role="navigation" runat="server">
        <div class="container">

            <!-- Site title -->
            <div class="navbar-header">
                <a class="navbar-brand" href="home.aspx">Tienda</a>
                <% if (session.IsUserLoggedIn())
                    { %> <span class="text-muted">Administración</span> <% } %>
            </div>

            <!-- Hamburguer -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#pagesItems">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Links -->
            <div class="collapse navbar-collapse justify-content-end" id="pagesItems">
                <ul class="nav navbar-nav">
                    <li class="nav-item"><span id="welcomeText" class="navbar-text text-capitalize" runat="server"></span></li>
                    <li class="nav-item"><a class="nav-link" href="ver.aspx">Ver</a></li>
                    <li><a class="nav-link" href="login.aspx"><i class="fas fa-sign-in-alt pr-1"></i>Acceder</a></li>
                    <li>
                        <asp:LinkButton class="nav-link" ID="logoutNavigation" runat="server" OnClick="Logout"><i class="fas fa-sign-out-alt pr-1"></i>Salir</asp:LinkButton>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
