﻿using FinalProject_ASP.main.models;
using MySql.Data.MySqlClient;
using System;
using System.Web.UI;
using static FinalProject_ASP.main.ver;

namespace FinalProject_ASP.main.user_controls.actions.delete
{
    public partial class delete_product_control : UserControl
    {
        //
        // Attributes
        //
        DatabaseService db;

        //
        // Methods
        //
        private void LoadIDs()
        {
            deleteProductId.Items.Clear();

            try
            {
                foreach (Producto p in db.GetProductos())
                    deleteProductId.Items.Add(p.id.ToString());

            } catch(MySqlException)
            {
                ShowModalWithID(this, "errorModal");
            }
        }
        private void UpdateData()
        {
            try
            {
                Producto actual = db.GetProductoById(Convert.ToInt32(deleteProductId.Text));

                deleteProductNameInput.Text = actual.nombre;
                deleteCostInput.Text = actual.precio.ToString();

                deleteProductManufacturerInput.Text = db.GetFabricanteNombreById(actual.idFabricante);

            } catch(SystemException) // This catches both MySqlException and FormatException, which are both issues by a database error in this case
            {
                ShowModalWithID(this, "errorModal");
            }
        }

        //
        // Listeners/Events
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                db = new DatabaseService();

                if (ShouldLoad || deleteProductId.Items.Count == 0)
                {
                    LoadIDs();
                    UpdateData();

                }

            }
            catch (MySqlException) {
                ShowModalWithID(this, "errorModal");
            }
        }

        protected void DeleteProduct(object sender, EventArgs e)
        {
            if (productConfirmDeletion.Checked)
            {
                db.DeleteProducto(Convert.ToInt32(deleteProductId.Text));
                CloseModalWithID(this, "deleteProductModal");
            }
            ShouldLoad = true;
        }

        protected void productConfirmDeletion_CheckedChanged(object sender, EventArgs e)
        {
            if (productConfirmDeletion.Checked)
                btnDeleteProduct.Attributes.Remove("disabled");
            else
                btnDeleteProduct.Attributes.Add("disabled", "disabled");
        }

        protected void deleteProductId_SelectionChanged(object sender, EventArgs e)
        {
            UpdateData();
        }
    }
}