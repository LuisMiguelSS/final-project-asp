﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="delete-manufacturer_control.ascx.cs" Inherits="FinalProject_ASP.main.user_controls.actions.delete.delete_manufacturer_control" %>

<div class="modal fade" id="deleteManufacturerModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-danger">
                <h4 class="modal-title text-white">Eliminar Fabricante</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <asp:UpdatePanel ID="UpdatePanelDeleteManufacturer" runat="server">
                <ContentTemplate>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="card mt-4">
                            <article class="card-body bg-light">
                                <!-- Id -->
                                <div class="form-group">
                                    <label for="removeManufacturerId">Identificador del fabricante</label>
                                    <asp:DropDownList class="custom-select" ID="removeManufacturerId" runat="server" OnSelectedIndexChanged="removeManufacturerId_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>

                                <!-- Name -->
                                <div class="form-group">
                                    <label for="editManufacturerNameInput">Nombre del fabricante</label>
                                    <asp:TextBox ID="deleteManufacturerNameInput" class="form-control" placeholder="Fabricante" type="text" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>

                                <!-- Confirm -->
                                <asp:CheckBox ID="manufacturerConfirmDeletion" runat="server" Text="Confirmar eliminación" OnCheckedChanged="manufacturerConfirmDeletion_CheckedChanged" AutoPostBack="true" />

                            </article>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnDeleteManufacturer" class="btn btn-danger" runat="server" OnClick="DeleteManufacturer" Text="Eliminar" disabled="disabled"></asp:Button>
                    </div>
                </ContentTemplate>

                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="removeManufacturerId" EventName="SelectedIndexChanged" />
                </Triggers>

            </asp:UpdatePanel>

        </div>
    </div>
</div>
