﻿using FinalProject_ASP.main.models;
using MySql.Data.MySqlClient;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using static FinalProject_ASP.main.ver;

namespace FinalProject_ASP.main.user_controls.actions.delete
{
    public partial class delete_manufacturer_control : UserControl
    {
        //
        // Attributes
        //
        DatabaseService db;

        //
        // Methods
        //
        private void LoadIDs()
        {
            removeManufacturerId.Items.Clear();

            foreach (Fabricante f in db.GetFabricantes())
            {
                removeManufacturerId.Items.Add(new ListItem(f.id.ToString(), f.name));
            }
        }
        private void UpdateName()
        {
            deleteManufacturerNameInput.Text = removeManufacturerId.SelectedValue;
        }

        //
        // Listeners/Events
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            db = new DatabaseService();

            try
            {
                if (ShouldLoad)
                {
                    LoadIDs();
                    UpdateName();
                }
            }
            catch (MySqlException) { }
        }

        protected void DeleteManufacturer(object sender, EventArgs e)
        {
            if (manufacturerConfirmDeletion.Checked)
            {
                try
                {
                    db.DeleteFabricante(Convert.ToInt32(removeManufacturerId.SelectedItem.Text));
                    LoadIDs();
                    UpdateName();
                    manufacturerConfirmDeletion.Checked = false;
                }
                catch (FormatException) { }
                finally
                {
                    CloseModalWithID(this, "deleteManufacturerModal");
                }
            }
            ShouldLoad = true;
        }

        protected void manufacturerConfirmDeletion_CheckedChanged(object sender, EventArgs e)
        {
            if (manufacturerConfirmDeletion.Checked)
                btnDeleteManufacturer.Attributes.Remove("disabled");
            else
                btnDeleteManufacturer.Attributes.Add("disabled", "disabled");
        }

        protected void removeManufacturerId_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateName();
        }
    }
}