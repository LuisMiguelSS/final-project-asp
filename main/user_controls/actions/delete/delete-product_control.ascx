﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="delete-product_control.ascx.cs" Inherits="FinalProject_ASP.main.user_controls.actions.delete.delete_product_control" %>

<div class="modal fade" id="deleteProductModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-danger">
                <h4 class="modal-title text-white">Eliminar Producto</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <asp:UpdatePanel ID="UpdatePanelDeleteProduct" runat="server">
                <ContentTemplate>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="card mt-4">
                            <article class="card-body bg-light">

                                <!-- Id -->
                                <div class="form-group">
                                    <label for="deleteProductId">Identificador del producto</label>
                                    <asp:DropDownList class="custom-select" ID="deleteProductId" runat="server" OnSelectedIndexChanged="deleteProductId_SelectionChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>

                                <!-- Name -->
                                <div class="form-group">
                                    <label for="deleteProductNameInput">Nombre del producto</label>
                                    <asp:TextBox ID="deleteProductNameInput" class="form-control" placeholder="Producto" type="text" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>

                                <!-- Cost -->
                                <div class="form-group">
                                    <label for="deleteCostInput">Precio del producto</label>
                                    <asp:TextBox ID="deleteCostInput" type="text" class="form-control col-sm-10 col-md-4" runat="server" ReadOnly="true" />
                                </div>

                                <!-- Manufacturer -->
                                <div class="form-group">
                                    <label for="deleteProductManufacturerInput">Fabricante del producto</label>
                                    <asp:TextBox class="custom-select" ID="deleteProductManufacturerInput" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>

                                <!-- Confirm -->
                                <asp:CheckBox ID="productConfirmDeletion" runat="server" Text="Confirmar eliminación" OnCheckedChanged="productConfirmDeletion_CheckedChanged" AutoPostBack="true" />

                            </article>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnDeleteProduct" class="btn btn-danger" runat="server" OnClick="DeleteProduct" Text="Eliminar" disabled="disabled"></asp:Button>
                    </div>
                </ContentTemplate>

                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="deleteProductId" EventName="SelectedIndexChanged" />
                </Triggers>

            </asp:UpdatePanel>

        </div>
    </div>
</div>
