﻿using FinalProject_ASP.main.models;
using MySql.Data.MySqlClient;
using System;
using System.Web.UI.WebControls;
using static FinalProject_ASP.main.ver;

namespace FinalProject_ASP.main.user_controls.actions.edit
{
    public partial class edit_product_control : System.Web.UI.UserControl
    {
        //
        // Attributes
        //
        DatabaseService db;

        //
        // Methods
        //
        private void LoadIDs()
        {
            editProductId.Items.Clear();

            try
            {
                foreach (Producto p in db.GetProductos())
                    editProductId.Items.Add(p.id.ToString());

            } catch(MySqlException)
            {
                ShowModalWithID(this, "errorModal");
            }
        }
        private void UpdateData()
        {
            try
            {
                Producto actual = db.GetProductoById(Convert.ToInt32(editProductId.Text));

                if (actual != null)
                {
                    editProductNameInput.Text = actual.nombre;
                    editCostInput.Value = actual.precio.ToString().Replace(',', '.');

                    editProductManufacturerInput.Items.Clear();
                    foreach (Fabricante f in db.GetFabricantes())
                        editProductManufacturerInput.Items.Add(f.name);

                    foreach (ListItem valor in editProductManufacturerInput.Items)
                        if (valor.Text.Equals(db.GetFabricanteNombreById(actual.idFabricante)))
                            editProductManufacturerInput.SelectedValue = db.GetFabricanteNombreById(actual.idFabricante);
                }

            }
            catch (SystemException) // This catches both MySqlException and FormatException, which are both issues by a database error in this case
            {
                ShowModalWithID(this, "errorModal");
            }
        }

        //
        // Listeners/Events
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            db = new DatabaseService();

            try
            {
                if (ShouldLoad || editProductId.Items.Count == 0)
                {
                    LoadIDs();
                    UpdateData();

                    ShouldLoad = false;
                }

            }
            catch (MySqlException)
            {
                ShowModalWithID(this, "errorModal");
            }
        }

        protected void EditProduct(object sender, EventArgs e)
        {
            ShouldLoad = true;
            
            try
            {
                db.UpdateProducto(Convert.ToInt32(editProductId.Text),
                                  editProductNameInput.Text,
                                  Convert.ToDouble(editCostInput.Value.Replace('.', ',')),
                                  db.GetFabricanteIdByNombre(editProductManufacturerInput.Text)
                                  );

                CloseModalWithID(this, "editProductModal");

            }
            catch (SystemException) // This catches both MySqlException and FormatException, which are both issues by a database error in this case
            {
                ShowModalWithID(this, "errorModal");
            }
        }

        protected void editProductId_SelectionChanged(object sender, EventArgs e)
        {
            ShouldLoad = false;
            UpdateData();
        }
    }
}