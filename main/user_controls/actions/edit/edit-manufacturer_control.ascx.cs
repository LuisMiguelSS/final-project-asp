﻿using FinalProject_ASP.main.models;
using MySql.Data.MySqlClient;
using System;
using System.Web.UI.WebControls;
using static FinalProject_ASP.main.ver;

namespace FinalProject_ASP.main.user_controls.actions.edit
{
    public partial class edit_manufacturer_control : System.Web.UI.UserControl
    {
        //
        // Attributes
        //
        DatabaseService db;

        //
        // Methods
        //
        private void LoadIDs()
        {
            editManufacturerId.Items.Clear();

            foreach (Fabricante f in db.GetFabricantes())
            {
                editManufacturerId.Items.Add(new ListItem(f.id.ToString(), f.name));
            }
        }
        private void UpdateName()
        {
            editManufacturerNameInput.Text = editManufacturerId.SelectedValue;
        }

        //
        // Listeners/Events
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            db = new DatabaseService();
            try
            {
                if (ShouldLoad)
                {
                    LoadIDs();
                    UpdateName();

                    ShouldLoad = false;
                }
            }
            catch (MySqlException) { }
        }

        protected void EditManufacturer(object sender, EventArgs e)
        {
            try
            {
                ShouldLoad = true;
                db.UpdateFabricante(Convert.ToInt32(editManufacturerId.SelectedItem.Text), editManufacturerNameInput.Text);
            }
            catch (FormatException) { }
            finally
            {
                CloseModalWithID(this, "editManufacturerModal");
            }
        }

        protected void editManufacturerId_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShouldLoad = false;
            UpdateName();
        }
    }
}