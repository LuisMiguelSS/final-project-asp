﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="edit-manufacturer_control.ascx.cs" Inherits="FinalProject_ASP.main.user_controls.actions.edit.edit_manufacturer_control" EnableViewState="true" %>

<div class="modal fade" id="editManufacturerModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <h4 class="modal-title text-white">Editar Fabricante</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="card mt-4">
                    <article class="card-body bg-light">

                        <asp:UpdatePanel ID="UpdatePanelEditManufacturer" runat="server">
                            <ContentTemplate>

                                <!-- Id -->
                                <div class="form-group">
                                    <label for="editManufacturerId">Identificador del fabricante</label>
                                    <asp:DropDownList class="custom-select" ID="editManufacturerId" runat="server" OnSelectedIndexChanged="editManufacturerId_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>

                                <!-- Name -->
                                <div class="form-group">
                                    <label for="editManufacturerNameInput">Nombre del fabricante</label>
                                    <asp:TextBox ID="editManufacturerNameInput" class="form-control" placeholder="Fabricante" type="text" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="EditManufacturerNameRequiredValidator" runat="server" ErrorMessage="Debes completar este campo." ControlToValidate="editManufacturerNameInput" CssClass="text-danger" ValidationGroup="FabricanteEdit"></asp:RequiredFieldValidator>
                                </div>
                            </ContentTemplate>

                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="editManufacturerId" EventName="SelectedIndexChanged" />
                            </Triggers>

                        </asp:UpdatePanel>

                    </article>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
                <asp:Button class="btn btn-success" runat="server" OnClick="EditManufacturer" Text="Guardar" ValidationGroup="FabricanteEdit"></asp:Button>
            </div>

        </div>
    </div>
</div>
