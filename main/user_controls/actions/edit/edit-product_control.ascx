﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="edit-product_control.ascx.cs" Inherits="FinalProject_ASP.main.user_controls.actions.edit.edit_product_control" %>

<div class="modal fade" id="editProductModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <h4 class="modal-title text-white">Editar Producto</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="card mt-4">
                    <article class="card-body bg-light">
                        <asp:UpdatePanel ID="UpdatePanelEditProduct" runat="server">
                            <ContentTemplate>
                                <!-- Id -->
                                <div class="form-group">
                                    <label for="editProductId">Identificador del producto</label>
                                    <asp:DropDownList class="custom-select" ID="editProductId" runat="server" OnSelectedIndexChanged="editProductId_SelectionChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>

                                <!-- Name -->
                                <div class="form-group">
                                    <label for="editProductNameInput">Nombre del producto</label>
                                    <asp:TextBox ID="editProductNameInput" class="form-control" placeholder="Producto" type="text" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="EditProductNameRequiredValidator" runat="server" ErrorMessage="Debes completar este campo." ControlToValidate="editProductNameInput" CssClass="text-danger" ValidationGroup="ProductEdit"></asp:RequiredFieldValidator>
                                </div>

                                <!-- Cost -->
                                <div class="form-group">
                                    <label for="editCostInput">Precio del producto</label>
                                    <input id="editCostInput" type="number" value="1" min="0" max="15000.99" pattern="[0-9]+([\.,][0-9]+)?" step="0.01" class="form-control col-sm-10 col-md-4" runat="server" />
                                </div>

                                <!-- Manufacturer -->
                                <div class="form-group">
                                    <label for="editProductManufacturerInput">Fabricante del producto</label>
                                    <asp:DropDownList class="custom-select" ID="editProductManufacturerInput" runat="server"></asp:DropDownList>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </article>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
                <asp:Button class="btn btn-success" runat="server" OnClick="EditProduct" Text="Guardar" ValidationGroup="ProductEdit"></asp:Button>
            </div>

        </div>
    </div>
</div>
