﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="add-manufacturer_control.ascx.cs" Inherits="FinalProject_ASP.main.user_controls.actions.add_manufacturer_control" %>

<div class="modal fade" id="addManufacturerModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-success">
                <h4 class="modal-title text-white">Añadir Fabricante</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="card mt-4">
                    <article class="card-body bg-light">

                        <!-- Name -->
                        <div class="form-group">
                            <label for="manufacturerNameInput">Nombre del fabricante</label>
                            <asp:TextBox ID="manufacturerNameInput" class="form-control" placeholder="Fabricante" type="text" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ManufacturerNameRequiredValidator" runat="server" ErrorMessage="Debes completar este campo." ControlToValidate="manufacturerNameInput" CssClass="text-danger" ValidationGroup="FabricanteNew"></asp:RequiredFieldValidator>
                        </div>

                    </article>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
                <asp:Button class="btn btn-success" runat="server" OnClick="AddNewManufacturer" Text="Guardar" ValidationGroup="FabricanteNew"></asp:Button>
            </div>

        </div>
    </div>
</div>
