﻿using FinalProject_ASP.main.models;
using MySql.Data.MySqlClient;
using System;
using static FinalProject_ASP.main.ver;

namespace FinalProject_ASP.main.user_controls.actions
{
    public partial class add_product_control : System.Web.UI.UserControl
    {
        //
        // Attributes
        //
        DatabaseService db;

        //
        // Constructor
        //
        public add_product_control()
        {
            db = new DatabaseService();
        }

        //
        // Listeners/events
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                foreach (Fabricante f in db.GetFabricantes())
                    manufacturerInput.Items.Add(f.name);
            }
            catch (MySqlException) {
                ShowModalWithID(this, "errorModal");
            }
        }

        protected void AddNewProduct(object sender, EventArgs e)
        {
            try
            {
                db.AddProduct(new Producto(0,
                                           nameInput.Text,
                                           Convert.ToDouble(costInput.Value.Replace('.', ',')),
                                           db.GetFabricanteIdByNombre(manufacturerInput.Text)
                                           )
                    );

                nameInput.Text = string.Empty;
                costInput.Value = "1";
                CloseModalWithID(this, "addProductModal");

            } catch(MySqlException)
            {
                ShowModalWithID(this, "errorModal");
            }
        }
    }
}