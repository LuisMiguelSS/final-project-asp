﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="add-product_control.ascx.cs" Inherits="FinalProject_ASP.main.user_controls.actions.add_product_control" %>

<div class="modal fade" id="addProductModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-success">
                <h4 class="modal-title text-white">Añadir Producto</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="card mt-4">
                    <article class="card-body bg-light">

                        <!-- Name -->
                        <div class="form-group">
                            <label for="nameInput">Nombre del producto</label>
                            <asp:TextBox ID="nameInput" class="form-control" placeholder="Producto" type="text" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ProductNameRequiredValidator" runat="server" ErrorMessage="Debes completar este campo." ControlToValidate="nameInput" CssClass="text-danger" ValidationGroup="ProductoNew"></asp:RequiredFieldValidator>
                        </div>

                        <!-- Cost -->
                        <div class="form-group">
                            <label for="costInput">Precio del producto</label>
                            <input id="costInput" type="number" value="1" min="0" max="15000.99" pattern="[0-9]+([\.,][0-9]+)?" step="0.01" class="form-control col-sm-10 col-md-4" runat="server"/>
                        </div>

                        <!-- Manufacturer -->
                        <div class="form-group">
                            <label for="manufacturerInput">Fabricante del producto</label>
                            <asp:DropDownList class="custom-select" ID="manufacturerInput" runat="server"></asp:DropDownList>
                        </div>

                    </article>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
                <asp:Button class="btn btn-success" runat="server" OnClick="AddNewProduct" Text="Guardar" ValidationGroup="ProductoNew"></asp:Button>
            </div>

        </div>
    </div>
</div>
