﻿using FinalProject_ASP.main.models;
using MySql.Data.MySqlClient;
using System;
using static FinalProject_ASP.main.ver;

namespace FinalProject_ASP.main.user_controls.actions
{
    public partial class add_manufacturer_control : System.Web.UI.UserControl
    {
        //
        // Attributes
        //
        DatabaseService db;

        //
        // Listeners/Events
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            db = new DatabaseService();
        }


        protected void AddNewManufacturer(object sender, EventArgs e)
        {
            try
            {
                db.AddFabricante(new Fabricante(0, manufacturerNameInput.Text));
                manufacturerNameInput.Text = string.Empty;

            } catch(MySqlException) { }
            finally
            {
                CloseModalWithID(this, "addManufacturerModal");
            }

        }
    }
}