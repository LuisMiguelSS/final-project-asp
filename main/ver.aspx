﻿<%@ Page Title="Ver - Tienda" Language="C#" MasterPageFile="~/main/template.Master" AutoEventWireup="true" CodeBehind="ver.aspx.cs" Inherits="FinalProject_ASP.main.ver" %>

<%@ Register Src="~/main/user_controls/actions/add/add-product_control.ascx" TagPrefix="uc" TagName="addproduct_control" %>
<%@ Register Src="~/main/user_controls/actions/add/add-manufacturer_control.ascx" TagPrefix="uc" TagName="addmanufacturer_control" %>
<%@ Register Src="~/main/user_controls/actions/edit/edit-manufacturer_control.ascx" TagPrefix="uc" TagName="editmanufacturer_control" %>
<%@ Register Src="~/main/user_controls/actions/edit/edit-product_control.ascx" TagPrefix="uc" TagName="editproduct_control" %>
<%@ Register Src="~/main/user_controls/actions/delete/delete-manufacturer_control.ascx" TagPrefix="uc" TagName="deletemanufacturer_control" %>
<%@ Register Src="~/main/user_controls/actions/delete/delete-product_control.ascx" TagPrefix="uc" TagName="deleteproduct_control" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <% if (session.IsUserLoggedIn())
        { %>
    <!-- Modals for addition, modification and deletion -->
    <!-- Manufacturer -->
    <uc:addmanufacturer_control runat="server" ID="addmanufacturer_control" />
    <uc:deletemanufacturer_control runat="server" ID="deletemanufacturer_control" />
    <uc:editmanufacturer_control runat="server" ID="editmanufacturer_control" />
    <!-- Product -->
    <uc:addproduct_control runat="server" ID="addproduct_control" />
    <uc:deleteproduct_control runat="server" ID="deleteproduct_control" />
    <uc:editproduct_control runat="server" ID="editproduct_control" />
    <% } %>

    <!-- Menu -->
    <div class="container mt-3">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#fabricantes">Fabricantes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#productos">Productos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#avanzado">Consultas</a>
            </li>
        </ul>

        <!-- Tabs -->
        <div class="tab-content">
            <div id="fabricantes" class="container tab-pane active">
                <br>
                <h3>Fabricantes
                    
                    <!-- Add/modify/delete -->
                    <% if (session.IsUserLoggedIn())
                        { %>
                    <asp:LinkButton runat="server" OnCommand="addManufacturerClick" class="btn bg-success text-white float-right"><i class="fas fa-plus-square pr-1"></i>Nuevo</asp:LinkButton>
                    <asp:LinkButton runat="server" OnCommand="editManufacturerClick" class="btn bg-warning text-white float-right"><i class="fas fa-pen pr-1"></i>Editar</asp:LinkButton>
                    <asp:LinkButton runat="server" OnCommand="deleteManufacturerClick" class="btn bg-danger text-white float-right"><i class="fas fa-trash pr-1"></i>Eliminar</asp:LinkButton>
                    <% } %>

                </h3>

                <!-- Table Manufacturer -->
                <asp:Repeater ID="ManufacturerRepeater" runat="server" OnItemDataBound="Manufacturers_DataBound">

                    <HeaderTemplate>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>

                    <ItemTemplate>
                        <tr>
                            <th scope="row"><%# DataBinder.Eval(Container.DataItem, "id") %></th>
                            <td><%# DataBinder.Eval(Container.DataItem, "name") %></td>
                        </tr>
                    </ItemTemplate>

                    <FooterTemplate>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="labelEmptyManufacturers"
                                    Text="No hay información." runat="server" Visible="false">
                                </asp:Label>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>

            </div>
            <div id="productos" class="container tab-pane fade">
                <br>
                <h3>Productos

                    <!-- Add/modify/delete -->
                    <% if (session.IsUserLoggedIn())
                        { %>
                    <asp:LinkButton runat="server" OnCommand="addProductClick" class="btn bg-success text-white float-right"><i class="fas fa-plus-square pr-1"></i>Nuevo</asp:LinkButton>
                    <asp:LinkButton runat="server" OnCommand="editProductClick" class="btn bg-warning text-white float-right"><i class="fas fa-pen pr-1"></i>Editar</asp:LinkButton>
                    <asp:LinkButton runat="server" OnCommand="deleteProductClick" class="btn bg-danger text-white float-right"><i class="fas fa-trash pr-1"></i>Eliminar</asp:LinkButton>
                    <% } %>
                </h3>

                <!-- Table Products -->
                <asp:Repeater ID="ProductsRepeater" runat="server" OnItemDataBound="Products_DataBound">

                    <HeaderTemplate>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Fabricante</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>

                    <ItemTemplate>
                        <tr>
                            <th scope="row"><%# DataBinder.Eval(Container.DataItem, "id") %></th>
                            <td><%# DataBinder.Eval(Container.DataItem, "nombre") %></td>
                            <td><%# DataBinder.Eval(Container.DataItem, "precio") %></td>
                            <td><%# DataBinder.Eval(Container.DataItem, "idFabricante") %></td>
                        </tr>
                    </ItemTemplate>

                    <FooterTemplate>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="labelEmptyProducts"
                                    Text="No hay información." runat="server" Visible="false">
                                </asp:Label>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div id="avanzado" class="container tab-pane fade">
                <br>
                <h3>Consulta de información avanzada</h3>

                <asp:UpdatePanel ID="QueryUpdatePanel" runat="server">
                    <ContentTemplate>
                        <fieldset id="queryFieldset" runat="server">

                            <!-- Table selection -->
                            <div class="form-group row">
                                <label for="tableSelect" class="col-sm-2 col-form-label">Tabla</label>
                                <div class="col-sm-10 col-md-2">
                                    <asp:DropDownList class="custom-select" ID="tableSelect" AutoPostBack="true" OnSelectedIndexChanged="Table_Selected" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <!-- Column selection -->
                            <div class="form-group row">
                                <label for="columnSelect" class="col-sm-2 col-form-label">Campo</label>
                                <div class="col-sm-10 col-md-2">
                                    <asp:DropDownList class="custom-select" ID="columnSelect" AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <!-- Search field -->
                            <div class="form-group row">
                                <label for="txtSearch" class="col-sm-2 col-form-label">Valor de búsqueda</label>
                                <div class="col-sm-10 col-md-4">
                                    <asp:TextBox ID="txtSearch" runat="server" ToolTip="Búsqueda..." class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <!-- Search -->
                            <div class="form-group row">
                                <div class="col-sm-10 offset-sm-2">
                                    <asp:Button ID="searchResultsBtn" runat="server" Text="Realizar búsqueda" class="btn btn-primary" OnClick="SearchResults_Clicked" CausesValidation="False" />
                                </div>
                            </div>
                        </fieldset>

                        <!-- Result table -->
                        <asp:GridView ID="ResultsTableGrid" runat="server" CssClass="table table-striped table-hover">
                            <EmptyDataTemplate>
                                No se han encontrado resultados.
                            </EmptyDataTemplate>
                        </asp:GridView>

                    </ContentTemplate>

                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="tableSelect" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="columnSelect" EventName="SelectedIndexChanged" />
                    </Triggers>

                </asp:UpdatePanel>

            </div>
        </div>

        <div class="content">
            <p>Nota: es recomendado recargar la página tras una actualización para la correcta muestra de información.</p>
        </div>

    </div>
</asp:Content>
