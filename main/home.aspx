﻿<%@ Page Title="Home - Tienda" Language="C#" MasterPageFile="~/main/template.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="FinalProject_ASP.main.pages.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="jumbotron jumbotron-fluid bg-primary py-5 pl-4 text-white text-center">
        <h1 class="display-3">¡La mejor tienda!</h1>
        <h4>Disfruta de los mejores productos, de los mejores fabricantes, directamente en tus manos.</h4>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-12 mb-3">
                <h2>Sobre el proyecto</h2>
                <hr />
                <p>Este proyecto está realizado con ASP.NET v4.7.2.</p>
                <p>Contiene 8 controles de usuario (añadir, modificar y eliminar productos/fabricantes, así como el <kbd>header</kbd> y el <kbd>footer</kbd>), 1 página maestra (plantilla) y 3 WebForms individuales (home, login y ver).</p>
                <p>Se controla que el usuario no exista al igual que la contraseña sea incorrecta.</p>
                <p>Es posible que al seleccionar por primera vez un item de un desplegable, este no se actualice, he comprobado que pasaba algunas veces y otras no, por temas del PostBack de ASP, así lo que lo he controlado lo mejor que he podido.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-12 m-2">
                <h2>Estructura del proyecto</h2>
                <hr />
                <ul id="treeview">
                    <li><span class="caret caret-down">FinalProject_ASP</span>
                        <ul class="nested active">
                            <li>DatabaseService.asmx</li>
                            <li>SessionService.asmx</li>
                            <li><span class="caret">src</span>
                                <ul class="nested">
                                    <li><span class="caret">assets</span>
                                        <ul class="nested">
                                            <li>custom</li>
                                            <li>libraries</li>
                                            <li>sql</li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><span class="caret">main</span>
                                <ul class="nested">
                                    <li><span class="caret">models</span>
                                        <ul class="nested">
                                            <li>Database.cs</li>
                                            <li>Fabricante.cs</li>
                                            <li>Producto.cs</li>
                                        </ul>
                                    </li>
                                    <li><span class="caret">user_controls</span>
                                        <ul class="nested">
                                            <li><span class="caret">actions</span>
                                                <ul class="nested">
                                                    <li><span class="caret">add</span>
                                                        <ul class="nested">
                                                            <li>add-manufacturer_control.ascx</li>
                                                            <li>add-product_control.ascx</li>
                                                        </ul>
                                                    </li>
                                                    <li><span class="caret">edit</span>
                                                        <ul class="nested">
                                                            <li>edit-manufacturer_control.ascx</li>
                                                            <li>edit-product_control.ascx</li>
                                                        </ul>
                                                    </li>
                                                    <li><span class="caret">delete</span>
                                                        <ul class="nested">
                                                            <li>delete-manufacturer_control.ascx</li>
                                                            <li>delete-product_control.ascx</li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>footer_user-control.ascx</li>
                                            <li>header_user-control.ascx</li>
                                        </ul>
                                    </li>
                                    <li>template.Master</li>
                                    <li>ver.aspx</li>
                                    <li>home.aspx</li>
                                    <li>login.aspx</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</asp:Content>
