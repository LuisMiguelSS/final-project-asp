﻿using MySql.Data.MySqlClient;
using System;
using System.Web.UI;
using static FinalProject_ASP.main.ver;

namespace FinalProject_ASP.main
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        DatabaseService db;
        SessionService session;

        protected void Page_Load(object sender, EventArgs e)
        {
            db = new DatabaseService();
            session = new SessionService();
        }

        protected void TryLogin(object sender, EventArgs e)
        {
            Console.WriteLine("TryLogin");
            string username = usernameInput.Text;
            string password = passwordInput.Text;

            try
            {
                if(db.UserExists(username))
                {
                    if (db.Login(username, password))
                    {
                        // Check if there's an already logged in user
                        if (session.IsUserLoggedIn())
                            session.Close();

                        session.Start(username);
                        Response.Redirect("home.aspx");

                    } else
                    {
                        // Wrong Password
                        ScriptManager.RegisterStartupScript(this, GetType(), UniqueID, "swal({icon: 'error',title: 'Ups...',text: '¡Contraseña incorrecta!'}); ",
                            true);
                        
                    }

                } else
                {
                    // Wrong username
                    ScriptManager.RegisterStartupScript(this, GetType(), UniqueID, "swal({icon: 'error',title: 'Ups...',text: '¡El usuario no existe!'}); ",
                        true);
                }

            }
            catch (MySqlException)
            {
                ShowModalWithID(this, "errorModal");
            }
        }

        protected void Logout(object sender, EventArgs e)
        {
            session.Close();
            Response.Redirect("login.aspx");
        }
    }
}