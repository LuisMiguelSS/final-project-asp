﻿using FinalProject_ASP.main.models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject_ASP.main
{
    public partial class ver : Page
    {
        //
        // Attributes
        //
        DatabaseService db;
        public static bool ShouldLoad = true;

        public SessionService session { get; set; }

        //
        // Constructor
        //
        public ver()
        {
            session = new SessionService();
        }

        //
        // Other methods
        //
        private void UpdateQueryColumns()
        {
            columnSelect.Items.Clear();

            if (tableSelect.Text.ToLower().Contains("fabricante"))
                columnSelect.Items.Add("nombre");
            else
            {
                columnSelect.Items.Add("nombre");
                columnSelect.Items.Add("precio");
            }
        }
        public static void ShowModalWithID(Control control, string id)
        {
            ShouldLoad = true;
            ScriptManager.RegisterStartupScript(control, control.GetType(), control.UniqueID, "$('#" + id + "').modal('show');", true);
        }
        public static void CloseModalWithID(Control control, string id)
        {
            ShouldLoad = true;
            ScriptManager.RegisterStartupScript(control, control.GetType(), control.UniqueID, "$('#" + id + "').modal('hide');", true);
        }

        //
        // Events/Listeners
        //
        protected void Page_Load(object sender, EventArgs e)
        {
            db = new DatabaseService();

            ManufacturerRepeater.DataSource = new List<Fabricante>();
            ProductsRepeater.DataSource = new List<Producto>();

            try
            {

                // Fabricantes
                ManufacturerRepeater.DataSource = db.GetFabricantes();

                // Productos
                ProductsRepeater.DataSource = db.GetProductos();

                if (ShouldLoad && !IsPostBack)
                {
                    // Queries
                    tableSelect.Items.Clear();
                    tableSelect.Items.Add("Fabricante");
                    tableSelect.Items.Add("Producto");

                    columnSelect.Items.Clear();
                    UpdateQueryColumns();
                }

            }
            catch (MySqlException)
            {
                ShowModalWithID(this, "errorModal");
            }
            finally
            {
                ManufacturerRepeater.DataBind();
                ProductsRepeater.DataBind();

                // Check if they are empty (Database error or just empty)
                if (ManufacturerRepeater.Items.Count == 0 || ProductsRepeater.Items.Count == 0)
                {
                    tableSelect.Items.Clear();
                    columnSelect.Items.Clear();

                    // Table select
                    tableSelect.Items.Add("Sin contenido");

                    // Column select
                    columnSelect.Items.Add("Sin contenido");

                    // Search text
                    txtSearch.Text = "Bloqueado";

                    queryFieldset.Attributes.Add("disabled", "disabled");
                }
            }

        }

        //
        // Manufacturer
        protected void Manufacturers_DataBound(object sender, RepeaterItemEventArgs e)
        {
            if (ManufacturerRepeater.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Label lblFooter = (Label)e.Item.FindControl("labelEmptyManufacturers");
                    lblFooter.Visible = true;
                }
            }
        }

        //
        // Product
        protected void Products_DataBound(object sender, RepeaterItemEventArgs e)
        {
            if (ProductsRepeater.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Label lblFooter = (Label)e.Item.FindControl("labelEmptyProducts");
                    lblFooter.Visible = true;
                }
            }
        }

        //
        // Others
        protected void Table_Selected(object sender, EventArgs e)
        {
            UpdateQueryColumns();
        }

        protected void SearchResults_Clicked(object sender, EventArgs e)
        {
            try
            {

                DataTable result = new DataTable();
                result.Load(db.ReaderForSearch(tableSelect.Text, columnSelect.Text, txtSearch.Text));

                ResultsTableGrid.DataSource = result;
                ResultsTableGrid.DataBind();
            }
            catch (MySqlException) {
                ShowModalWithID(this, "errorModal");
            }
        }

        protected void addManufacturerClick(object sender, EventArgs e)
        {
            ShouldLoad = true;
            ShowModalWithID(this, "addManufacturerModal");
        }

        protected void editManufacturerClick(object sender, EventArgs e)
        {
            ShouldLoad = true;
            ShowModalWithID(this, "editManufacturerModal");
        }

        protected void deleteManufacturerClick(object sender, EventArgs e)
        {
            ShouldLoad = true;
            ShowModalWithID(this, "deleteManufacturerModal");
        }

        protected void addProductClick(object sender, CommandEventArgs e)
        {
            ShouldLoad = true;
            ShowModalWithID(this, "addProductModal");
        }

        protected void editProductClick(object sender, CommandEventArgs e)
        {
            ShouldLoad = true;
            ShowModalWithID(this, "editProductModal");
        }

        protected void deleteProductClick(object sender, CommandEventArgs e)
        {
            ShouldLoad = true;
            ShowModalWithID(this, "deleteProductModal");
        }
    }
}