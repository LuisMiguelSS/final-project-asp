﻿namespace FinalProject_ASP.main.models
{
    public class Fabricante
    {

        //
        // Atributos
        //
        public int id
        {
            get; set;
        }
        public string name
        {
            get; set;
        }

        //
        // Constructor
        //
        public Fabricante() : this(-1, "") { }
        public Fabricante(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}