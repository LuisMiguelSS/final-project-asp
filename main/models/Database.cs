﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace FinalProject_ASP.main.models
{
    public class Database
    {
        //
        // Atributos
        //
        public static readonly string SERVER = "127.0.0.1";
        public static readonly string DATABASE = "tienda";
        public static readonly string UID = "root";
        public static readonly string PASSWORD = "";
        private static Database singleton;

        //
        // Constructor
        //
        private Database() { }

        //
        // Methods
        //
        public static Database GetInstanceOf()
        {
            if (singleton == null)
                singleton = new Database();

            return singleton;
        }

        // DB & Utils Methods
        public MySqlConnection GetConexion()
        {
            return new MySqlConnection(
                "server=" + SERVER + ";" +
                "database=" + DATABASE + ";" +
                "Uid=" + UID + ";" +
                "pwd=" + PASSWORD + ";"
                );
        }

        public static string GetString(MySqlDataReader reader, string columnName)
        {
            return reader[columnName] == DBNull.Value ? "" : (string)reader[columnName];
        }

        public static double GetDouble(MySqlDataReader reader, string columnName)
        {
            return reader[columnName] == DBNull.Value ? -1.00 : (double)reader[columnName];
        }

        //
        // Fabricantes

        public List<Fabricante> GetFabricantes()
        {
            List<Fabricante> fabricantes = new List<Fabricante>();

            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                using (MySqlCommand command = new MySqlCommand("SELECT * FROM fabricante ORDER BY codigo", conn))
                {
                    using (MySqlDataReader r = command.ExecuteReader())
                    {
                        while (r.Read())
                            fabricantes.Add(new Fabricante(
                                r.GetInt32("codigo"),
                                GetString(r, "nombre")
                                ));
                    }
                }

            }
            conn.Close();

            return fabricantes;
        }


        public int GetFabricanteIdByNombre(string nombre)
        {
            foreach (Fabricante f in GetFabricantes())
                if (f.name.ToLower().Contains(nombre.ToLower()))
                    return f.id;

            return -1;
        }


        public string GetFabricanteNombreById(int id)
        {
            foreach (Fabricante f in GetFabricantes())
                if (f.id == id)
                    return f.name;

            return string.Empty;
        }


        public bool AddFabricante(Fabricante fabricante)
        {
            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    // Check if exists
                    using (MySqlCommand c = new MySqlCommand("SELECT * FROM fabricante WHERE nombre = @1", conn))
                    {
                        c.Parameters.AddWithValue("@1", fabricante.name);

                        if (c.ExecuteReader().HasRows)
                            return false;
                    }

                    conn.Close();
                    conn.Open();

                    // Insert
                    using (MySqlCommand command = new MySqlCommand("INSERT INTO fabricante VALUES(NULL, @1)", conn))
                    {
                        command.Parameters.AddWithValue("@1", fabricante.name);

                        return command.ExecuteNonQuery() > 0;
                    }
                }
                catch (MySqlException) { return false; }

            }
            conn.Close();

            return false;
        }


        public bool UpdateFabricante(int id, string nombre)
        {
            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand c = new MySqlCommand("UPDATE fabricante SET nombre = @1 WHERE codigo = @2", conn))
                    {
                        c.Parameters.AddWithValue("@1", nombre);
                        c.Parameters.AddWithValue("@2", id);

                        return c.ExecuteNonQuery() > 0;
                    }

                }
                catch (MySqlException) { return false; }

            }
            conn.Close();

            return false;
        }


        public bool DeleteFabricante(int id)
        {
            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand c = new MySqlCommand("DELETE FROM fabricante WHERE codigo = @1", conn))
                    {
                        c.Parameters.AddWithValue("@1", id);

                        return c.ExecuteNonQuery() > 0;
                    }

                }
                catch (MySqlException) { return false; }

            }
            conn.Close();

            return false;
        }

        //
        // Productos

        public List<Producto> GetProductos()
        {
            List<Producto> fabricantes = new List<Producto>();

            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                using (MySqlCommand command = new MySqlCommand("SELECT * FROM producto ORDER BY codigo", conn))
                {
                    using (MySqlDataReader r = command.ExecuteReader())
                    {
                        while (r.Read())
                            fabricantes.Add(new Producto(
                                r.GetInt32("codigo"),
                                GetString(r, "nombre"),
                                GetDouble(r, "precio"),
                                r.GetInt32("codigo_fabricante")
                                ));
                    }
                }

            }
            conn.Close();

            return fabricantes;
        }


        public Producto GetProductoById(int id)
        {
            Producto producto = null;

            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                using (MySqlCommand command = new MySqlCommand("SELECT * FROM producto WHERE codigo = @1", conn))
                {
                    command.Parameters.AddWithValue("@1", id);

                    using (MySqlDataReader r = command.ExecuteReader())
                    {
                        while (r.Read())
                            producto = new Producto(
                                r.GetInt32("codigo"),
                                GetString(r, "nombre"),
                                GetDouble(r, "precio"),
                                r.GetInt32("codigo_fabricante")
                                );
                    }
                }

            }
            conn.Close();

            return producto;
        }


        public bool AddProduct(Producto producto)
        {
            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    // Check if exists
                    using (MySqlCommand c = new MySqlCommand("SELECT * FROM producto WHERE nombre = @1", conn))
                    {
                        c.Parameters.AddWithValue("@1", producto.nombre);

                        if (c.ExecuteReader().HasRows)
                            return false;
                    }

                    conn.Close();
                    conn.Open();

                    // Insert
                    using (MySqlCommand command = new MySqlCommand("INSERT INTO producto VALUES(NULL, @1, @2, @3)", conn))
                    {
                        command.Parameters.AddWithValue("@1", producto.nombre);
                        command.Parameters.AddWithValue("@2", producto.precio);
                        command.Parameters.AddWithValue("@3", producto.idFabricante);

                        return command.ExecuteNonQuery() > 0;
                    }
                }
                catch (MySqlException) { return false; }

            }
            conn.Close();

            return false;
        }


        public bool UpdateProducto(int id, string nombre, double precio, int fabricante)
        {
            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand c = new MySqlCommand("UPDATE producto SET nombre = @1, precio = @2, codigo_fabricante = @3 WHERE codigo = @4", conn))
                    {
                        c.Parameters.AddWithValue("@1", nombre);
                        c.Parameters.AddWithValue("@2", precio);
                        c.Parameters.AddWithValue("@3", fabricante);
                        c.Parameters.AddWithValue("@4", id);

                        return c.ExecuteNonQuery() > 0;
                    }

                }
                catch (MySqlException) { return false; }

            }
            conn.Close();

            return false;
        }


        public bool DeleteProducto(int id)
        {
            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand c = new MySqlCommand("DELETE FROM producto WHERE codigo = @1", conn))
                    {
                        c.Parameters.AddWithValue("@1", id);

                        return c.ExecuteNonQuery() > 0;
                    }

                }
                catch (MySqlException) { return false; }

            }
            conn.Close();

            return false;
        }

        //
        // Search
        //

        public MySqlDataReader ReaderForSearch(string table, string column, string value)
        {
            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                using (MySqlCommand command = new MySqlCommand("SELECT * FROM " + table + " WHERE " + column + " LIKE '%" + value + "%'", conn))
                {
                    return command.ExecuteReader();
                }

            }
            conn.Close();

            return null;
        }

        //
        // Administradores

        public bool Login(string usuario, string contrasena)
        {
            bool result = false;
            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                string SqlWhere = "usuario = '" + usuario + "' AND contrasena = '" + contrasena + "'";

                using (MySqlCommand command = new MySqlCommand("SELECT * FROM administradores WHERE " + SqlWhere, conn))
                {
                    using (MySqlDataReader r = command.ExecuteReader())
                    {
                        while (r.Read())
                            result = true;
                    }
                }

            }
            conn.Close();

            return result;
        }

        public bool UserExists(string username)
        {
            bool result = false;
            MySqlConnection conn = singleton.GetConexion();
            conn.Open();

            // Si la conexión está abierta, realizar consulta
            if (conn.State == ConnectionState.Open)
            {
                using (MySqlCommand command = new MySqlCommand("SELECT * FROM administradores WHERE usuario = '" + username + "'", conn))
                {
                    using (MySqlDataReader r = command.ExecuteReader())
                    {
                        while (r.Read())
                            result = true;
                    }
                }

            }
            conn.Close();

            return result;
        }
    }
}