﻿namespace FinalProject_ASP.main.models
{
    public class Producto
    {
        //
        // Atributos
        //
        public int id
        {
            get; set;
        }
        public string nombre
        {
            get; set;
        }
        public double precio
        {
            get; set;
        }
        public int idFabricante
        {
            get; set;
        }

        //
        // Constructor
        //
        public Producto() : this(-1, "", 0.00, -1) { }
        public Producto(int id, string nombre, double precio, int idFabricante)
        {
            this.id = id;
            this.nombre = nombre;
            this.precio = precio;
            this.idFabricante = idFabricante;
        }
    }
}